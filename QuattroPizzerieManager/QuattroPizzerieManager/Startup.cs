﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QuattroPizzerieManager.Startup))]
namespace QuattroPizzerieManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
